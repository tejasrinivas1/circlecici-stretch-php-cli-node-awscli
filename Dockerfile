FROM circleci/php:7.1.20-cli-stretch-node
# uses 
# - debian stretch: 9.5-slim 
# - node: 8.11.3
# - python: 2.7.13-2

ARG AWS_CLI_VERSION=1.15.73

USER root

RUN apt-get update && \
    apt-get install -y python-pip python-dev && \
    pip install --no-cache-dir awscli==${AWS_CLI_VERSION} python-magic
